package ba.etf.rma21.projekat.data.models

class OdgovorPOST (
    val pitanje: Int,
    val odgovor: Int,
    val bodovi: Number
)