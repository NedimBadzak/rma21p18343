package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.dao.AccountDAO
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class AccountRepository {
    companion object {
        var acHash: String = "8a42895c-fffd-44df-a847-73917fcf52f1"

        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun postaviHash(acHash: String): Boolean {
            this.acHash = acHash
            return withContext(Dispatchers.IO) {
                val calendar = Calendar.getInstance().time
                val sdf = SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss")
                val db = AppDatabase.getInstance(context)
//                val account: Account = db!!.accountDao().getStudent(acHash)
                db!!.accountDao().deleteAll()
                db!!.accountDao().upisi(Account(acHash, sdf.format(calendar)))
                return@withContext true
            }
        }


        fun getHash(): String {
            return acHash
        }


        suspend fun getAccount(acHash: String): Account {
            return withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context)
                return@withContext db!!.accountDao().getStudent(acHash)
            }
        }

    }
}