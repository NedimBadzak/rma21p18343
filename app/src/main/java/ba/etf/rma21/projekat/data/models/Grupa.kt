package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Grupa(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "naziv") val naziv: String,
    @SerializedName("PredmetId") val predmetID: Int
) {
}