package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import ba.etf.rma21.projekat.viewmodel.KvizoviListViewModel
import kotlinx.coroutines.*

class FragmentKvizovi : Fragment() {
    lateinit var kvizovi: RecyclerView
    private lateinit var filteri: Spinner
    var kvizoviListViewModel = KvizoviListViewModel()
    private var kvizoviArrayList: List<Kviz> = listOf()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_kvizovi, container, false)
        kvizovi = view.findViewById(R.id.listaKvizova)
        filteri = view.findViewById(R.id.filterKvizova)
        kvizovi.layoutManager = GridLayoutManager(
            context,
            2
        )

        filteri.adapter = ArrayAdapter(
            context!!, R.layout.support_simple_spinner_dropdown_item, listOf(
            "Svi moji kvizovi",
            "Svi kvizovi",
            "Urađeni kvizovi",
            "Budući kvizovi",
            "Prošli kvizovi"

        ))
        filteri.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(parent?.context, "Niste označili opciju", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val scope = CoroutineScope(Job() + Dispatchers.Main)
                scope.launch {
                    kvizoviArrayList = kvizoviListViewModel.chooseKviz(position)!!
                    kvizovi.adapter = KvizoviListAdapter(kvizoviArrayList)
                }
            }
        }
        return view;
    }
    companion object {
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
    }
    override fun onResume() {
        super.onResume()
        val scope = CoroutineScope(Job() + Dispatchers.Main)
        scope.launch {
            kvizovi.adapter = KvizoviListAdapter(kvizoviListViewModel.chooseKviz(0)!! )
        }
        (activity as MainActivity).bottomNavigationView.setOnNavigationItemSelectedListener((activity as MainActivity).mOnNavigationItemSelectedListener)

    }

    public fun reBindNavListener() {
        (activity as MainActivity).bottomNavigationView.setOnNavigationItemSelectedListener((activity as MainActivity).mOnNavigationItemSelectedListener)
    }
}