package ba.etf.rma21.projekat.view

import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import ba.etf.rma21.projekat.viewmodel.KvizoviListViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjeViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class KvizoviListAdapter(
    var kvizovi: List<Kviz>
) : RecyclerView.Adapter<KvizoviListAdapter.KvizViewHolder>() {
    val sortirano = kvizovi.sortedBy { kviz -> kviz.datumPocetka }
    val pokusajFragmenti: MutableList<FragmentPokusaj> = mutableListOf()
    val rezultatFragmenti: MutableList<FragmentPoruka> = mutableListOf()
    val pitanjeViewModel = PitanjeViewModel()
    lateinit var fragmentKviz: Fragment
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_kviz, parent, false)
        return KvizViewHolder(view)
    }

    override fun onBindViewHolder(holder: KvizViewHolder, position: Int) {
        var kvizTaken: KvizTaken? = null
        CoroutineScope(Job() + Dispatchers.Main).launch {
            kvizTaken = dajKvizTaken(sortirano[position])
            dodajSvePokusajFragmente(
                sortirano[position].id
            )
        }

        holder.nazivPredmeta.text = sortirano[position].listaPredmet
        holder.datumKviza.text = setDatumKviza(sortirano[position], kvizTaken)
        holder.statusImage.setImageDrawable(
            ResourcesCompat.getDrawable(
                holder.itemView.context.resources,
                setImageKviza(sortirano[position], kvizTaken),
                null
            )
        )
        holder.imeKviza.text = sortirano[position].naziv
        holder.trajanjeKviza.text = sortirano[position].trajanje.toString()
        holder.bodoviKviz.text = kvizTaken?.osvojeniBodovi?.toString() ?: "\"\""

        val mojActivity: MainActivity = holder.itemView.context as MainActivity
        holder.itemView.setOnClickListener {
            val scope = CoroutineScope(Job() + Dispatchers.Main)
            scope.launch {
                Log.v("TAGIC", "Pritisnuto")
                val date: Date = Calendar.getInstance().time
                val kvizoviListViewModel: KvizoviListViewModel = KvizoviListViewModel()
                if (date.after(SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(sortirano[position].datumPocetka)) && (sortirano[position].datumKraj == null || date.before(
                        SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(sortirano[position].datumKraj)
                    )
                            )
                ) { //zeleno
                    Log.v("TAGIC", mojActivity.supportFragmentManager.fragments.size.toString())
                    Log.v("TAGIC pokusajFragmenti size", pokusajFragmenti.size.toString())
                    mojActivity.openFragment(pokusajFragmenti[position])
                    fragmentKviz = mojActivity.supportFragmentManager.fragments.find {
                        it.tag.equals("ba.etf.rma21.projekat.view.FragmentKvizovi")
                    }!!
                    if (pokusajFragmenti[position].listaPitanja.isNotEmpty()) {
                        mojActivity.supportFragmentManager.beginTransaction().hide(fragmentKviz)
                            .commit()
                        mojActivity.supportFragmentManager.beginTransaction()
                            .show(pokusajFragmenti[position])
                            .addToBackStack(pokusajFragmenti[position].javaClass.name).commit()
                        PitanjeViewModel.pokrenutiKviz = sortirano[position]
                        kvizTaken = TakeKvizRepository.zapocniKviz(sortirano[position].id)
                        PitanjeViewModel.pokusaj = kvizTaken!!.id
                        if (PitanjeViewModel.listaZavrsenih[PitanjeViewModel.pokrenutiKviz] == null || PitanjeViewModel.listaZavrsenih[PitanjeViewModel.pokrenutiKviz] == false) {

                            mojActivity.bottomNavigationView.menu.setGroupVisible(
                                R.id.glavnaGrupa,
                                false
                            )
                            mojActivity.bottomNavigationView.menu.setGroupVisible(
                                R.id.kvizGrupa,
                                true
                            )
                        } else {
                            mojActivity.bottomNavigationView.menu.setGroupVisible(
                                R.id.glavnaGrupa,
                                true
                            )
                            mojActivity.bottomNavigationView.menu.setGroupVisible(
                                R.id.kvizGrupa,
                                false
                            )
                        }

                        mojActivity.bottomNavigationView.setOnNavigationItemSelectedListener {
                            if (it.itemId == R.id.kvizovi) {
                                mojActivity.openFragment(fragmentKviz)
                            } else if (it.itemId == R.id.predmeti) {
                                mojActivity.openFragment(mojActivity.predmetiFragment)
                            } else if (it.itemId == R.id.predajKviz) {
                                PitanjeViewModel.kvizPitanjaRezultati.put(
                                    PitanjeViewModel.pokrenutiKviz,
                                    PitanjeViewModel.pitanjaOdgovori
                                )

                                val transaction =
                                    mojActivity.supportFragmentManager.beginTransaction()
                                for (i in 0 until pokusajFragmenti[position].fragmentPitanja.size) {
                                    val frag = pokusajFragmenti[position].fragmentPitanja[i]
                                    pokusajFragmenti[position].fragmentPitanja.forEach { it1 ->
                                        it1.odgovoriLista?.isEnabled = false
                                    }
                                    if (frag.isVisible) {
                                        val item =
                                            pokusajFragmenti[position].navigacijaPitanja.menu.getItem(
                                                i
                                            )
                                        val s = SpannableString(item.title)
                                        if (FragmentPitanje.odgovor != null) {
                                            val boja =
                                                if (FragmentPitanje.odgovor!!) mojActivity.resources.getColor(
                                                    R.color.zeleniodgovor,
                                                    null
                                                )
                                                else mojActivity.resources.getColor(
                                                    R.color.crveniodgovor,
                                                    null
                                                )
                                            s.setSpan(ForegroundColorSpan(boja), 0, s.length, 0)
                                            item.title = s
                                        }
                                        transaction.hide(frag)
                                        var sdf = SimpleDateFormat("dd.MM.yyyy")
                                        holder.statusImage.setImageDrawable(
                                            ResourcesCompat.getDrawable(
                                                holder.itemView.context.resources,
                                                R.drawable.plava,
                                                null
                                            )
                                        )


                                    }
                                }
                                for (i in 0 until pokusajFragmenti[position].navigacijaPitanja.menu.size()) pokusajFragmenti[position].navigacijaPitanja.menu.getItem(
                                    i
                                ).isEnabled = true
                                PitanjeViewModel.listaZavrsenih[sortirano[position]] = true
                                if (PitanjeViewModel.listaZavrsenih[PitanjeViewModel.pokrenutiKviz] == true) {
                                    mojActivity.bottomNavigationView.menu.setGroupVisible(
                                        R.id.glavnaGrupa,
                                        true
                                    )
                                    mojActivity.bottomNavigationView.menu.setGroupVisible(
                                        R.id.kvizGrupa,
                                        false
                                    )
                                }
                                mojActivity.bottomNavigationView.setOnNavigationItemSelectedListener(
                                    mojActivity.mOnNavigationItemSelectedListener
                                )

                                PitanjeViewModel.pitanjaOdgovori.clear()

                                rezultatFragmenti.add(FragmentPoruka("Završili ste kviz ${holder.imeKviza.text} sa tačnosti ${PitanjeViewModel.ostvareniBodovi}"))
                                val menuSize =
                                    pokusajFragmenti[position].navigacijaPitanja.menu.size()
                                pokusajFragmenti[position].navigacijaPitanja.menu.add(
                                    R.id.navGrupa,
                                    menuSize,
                                    menuSize,
                                    "Rezultat"
                                )
                                transaction.add(
                                    R.id.framePitanje,
                                    rezultatFragmenti[rezultatFragmenti.size - 1]
                                )
                                transaction.commit()
                                mojActivity.bottomNavigationView.setOnNavigationItemSelectedListener(
                                    mojActivity.mOnNavigationItemSelectedListener
                                )
                                it.isEnabled = false

                                return@setOnNavigationItemSelectedListener true
                            } else if (it.itemId == R.id.zaustaviKviz) {
                                mojActivity.supportFragmentManager.beginTransaction()
                                    .hide(pokusajFragmenti[position]).show(fragmentKviz).commit()
                                (fragmentKviz as FragmentKvizovi).reBindNavListener()
                                mojActivity.bottomNavigationView.menu.setGroupVisible(
                                    R.id.glavnaGrupa,
                                    true
                                )
                                mojActivity.bottomNavigationView.menu.setGroupVisible(
                                    R.id.kvizGrupa,
                                    false
                                )
                            }
                            false
                        }
                    } else Toast.makeText(mojActivity, "Taj kviz nema pitanja", Toast.LENGTH_SHORT)
                        .show()
                } else if (kvizTaken != null && SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kvizTaken!!.datumRada).before(date) && SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kvizTaken!!.datumRada).after(
                        SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(sortirano[position].datumPocetka)
                    )
                ) { //plavo
                    Toast.makeText(mojActivity, "Prikazivanje uradjenog kviza", Toast.LENGTH_SHORT)
                        .show()
                } else { //zuto ili crveno
                    Toast.makeText(
                        mojActivity,
                        "Nije moguće otvoriti taj kviz!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private suspend fun dajKvizTaken(kviz: Kviz): KvizTaken? {
        return TakeKvizRepository.getPocetiKvizovi()?.firstOrNull() {
            it.KvizId == kviz.id
        }
    }

    private suspend fun dodajSvePokusajFragmente(idKviza: Int) {
        pokusajFragmenti.add(
            FragmentPokusaj(
                PitanjeKvizRepository.getPitanja(idKviza)!!
            )
        )
    }

    override fun getItemCount(): Int = sortirano.size

    inner class KvizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val statusImage: ImageView = itemView.findViewById(R.id.statusImgView)
        val nazivPredmeta: TextView = itemView.findViewById(R.id.nazivPredmetaTextView)
        val imeKviza: TextView = itemView.findViewById(R.id.imeKvizaTextView)
        val datumKviza: TextView = itemView.findViewById(R.id.datumKvizaTextView)
        val trajanjeKviza: TextView = itemView.findViewById(R.id.trajanjeTextView)
        val bodoviKviz: TextView = itemView.findViewById(R.id.bodoviTextView)
    }

    private fun setDatumKviza(kviz: Kviz, kvizTaken: KvizTaken?): String? {
        val date: Date = Calendar.getInstance().time
        var sdf = SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss")
        return if (kvizTaken != null && SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kvizTaken.datumRada).before(date) && SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kvizTaken.datumRada).after(
                SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kviz.datumPocetka)
            )
        ) {
            kvizTaken.datumRada//PLAVA
        } else if (date.after(SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kviz.datumPocetka)) && (kviz.datumKraj == null || date.before(SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kviz.datumKraj)))) {
            if (kviz.datumKraj == null) "\"\"" else kviz.datumKraj
        } else if (date.before(SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kviz.datumPocetka))) {
            kviz.datumPocetka //ZUTA
        } else if (date.after(SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kviz.datumKraj))) {
            kviz.datumKraj //CRVENA
        } else {
            sdf.parse("1981-09-07T02:25:14").toString()
        }
    }

    private fun setImageKviza(kviz: Kviz, kvizTaken: KvizTaken?): Int {
        val date: Date = Calendar.getInstance().time
        return if (
            kvizTaken != null && (
                    SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kvizTaken.datumRada).after(SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kviz.datumPocetka)) || kvizTaken.datumRada.equals(kviz.datumPocetka))
        ) {
            R.drawable.plava
        } else if (date.after(SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kviz.datumPocetka)) && (kviz.datumKraj == null || date.before(SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kviz.datumKraj)))) {
            R.drawable.zelena
        } else if (date.before(SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kviz.datumPocetka))) {
            R.drawable.zuta
        } else if (date.after(SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss").parse(kviz.datumKraj))) {
            R.drawable.crvena
        } else {
            return R.color.black
        }
    }

    private fun dajPredmeteZarezom(list: List<Predmet>): String {
        var returner: String = ""
        for (i in list.indices) {
            returner += list[i].naziv
            if (i != list.size - 1) returner += ","
        }
        return returner
    }

    private suspend fun popuniListuPredmeta(kviz: Kviz): List<Predmet> {
        return PredmetIGrupaRepository.getGrupeByKviz(kviz.id).map {
            PredmetIGrupaRepository.getPredmetByID(it.predmetID)!!
        }.toSet().toList()
    }
}