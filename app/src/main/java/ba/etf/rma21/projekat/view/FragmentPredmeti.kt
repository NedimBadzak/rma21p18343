package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import ba.etf.rma21.projekat.viewmodel.PredmetListViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class FragmentPredmeti : Fragment() {

    lateinit var odabirGodina: Spinner
    lateinit var odabirGrupa: Spinner
    lateinit var odabirPredmet: Spinner
    lateinit var dodajPredmetDugme: Button
    var predmetListViewModel: PredmetListViewModel = PredmetListViewModel()
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_predmeti, container, false)
        odabirGodina = view.findViewById(R.id.odabirGodina)
        odabirGrupa = view.findViewById(R.id.odabirGrupa)
        odabirPredmet = view.findViewById(R.id.odabirPredmet)
        dodajPredmetDugme = view.findViewById(R.id.dodajPredmetDugme)
        dodajPredmetDugme.isEnabled = false


        odabirGodina.adapter = PredmetArrayAdapter(
            context!!, R.layout.support_simple_spinner_dropdown_item, listOf(
                "Godina",
                "1",
                "2",
                "3",
                "4",
                "5"
            )
        )
        odabirGodina.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    scope.launch {
//                        Log.d("MOJTAG upisaniPredmeti", PredmetRepository.getUpisani().toString())
                        if (predmetListViewModel.dajPredmete(position).isEmpty()) {
                            odabirPredmet.isEnabled = false
                            odabirGrupa.isEnabled = false
                        } else {
                            odabirPredmet.isEnabled = true
                            odabirGrupa.isEnabled = true
                        }

                        odabirPredmet.adapter = ArrayAdapter(
                            parent?.context!!,
                            R.layout.support_simple_spinner_dropdown_item,
                            predmetListViewModel.dajPredmete(position)
                        )
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(parent?.context, "Niste označili opciju", Toast.LENGTH_SHORT)
                    .show()
            }
        }
        odabirPredmet.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                scope.launch {
                    odabirGrupa.adapter = ArrayAdapter(
                        parent?.context!!,
                        R.layout.support_simple_spinner_dropdown_item,
                        predmetListViewModel.dajGrupe(odabirPredmet.selectedItem.toString())
                    )
                    dodajPredmetDugme.isEnabled = true
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(parent?.context, "Niste označili opciju", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        dodajPredmetDugme.setOnClickListener {
            scope.launch {

                predmetListViewModel.dodaj(
                    odabirGodina.selectedItem.toString(),
                    odabirPredmet.selectedItem.toString(),
                    odabirGrupa.selectedItem.toString()
                )
            }
            val transaction = activity!!.supportFragmentManager.beginTransaction()
            transaction.replace(
                R.id.container,
                FragmentPoruka("Uspješno ste upisani u grupu ${odabirGrupa.selectedItem.toString()} predmeta ${odabirPredmet.selectedItem.toString()}!")
            )
            transaction.commit()
        }
        return view
    }

    companion object {
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
    }

}