package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Kviz(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "naziv") val naziv: String,
    @ColumnInfo(name = "datumPocetka") @SerializedName("datumPocetak") val datumPocetka: String,
    @ColumnInfo(name = "datumKraj") val datumKraj: String,
    @ColumnInfo(name = "trajanje") val trajanje: Int,
    @ColumnInfo(name = "listaPredmet") var listaPredmet: String
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Kviz

        if (id != other.id) return false
        if (naziv != other.naziv) return false
        if (datumPocetka != other.datumPocetka) return false
        if (datumKraj != other.datumKraj) return false
        if (trajanje != other.trajanje) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + naziv.hashCode()
        result = 31 * result + datumPocetka.hashCode()
        result = 31 * result + (datumKraj?.hashCode() ?: 0)
        result = 31 * result + trajanje
        return result
    }
}