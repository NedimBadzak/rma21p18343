package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import android.widget.Toast
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DBRepository {
    companion object{
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun updateNow() : Boolean{
            return withContext(Dispatchers.IO) {
                AccountRepository.postaviHash(AccountRepository.acHash)
                val account = AccountRepository.getAccount(AccountRepository.acHash)
                Log.d("TAGIC", account.acHash)
                var response = ApiAdapter.retrofit.getChanged(account.acHash, account.lastUpdate)
                if(response.toString().contains("true")) {
//                    obrisiSve()
//                    ubaciSve()
//                    AccountRepository.postaviHash(AccountRepository.acHash)
                    return@withContext true
                }
                return@withContext false
            }
        }

//        private suspend fun ubaciSve() {
//            val db = AppDatabase.getInstance(context)
////                val account: Account = db!!.accountDao().getStudent(acHash)
//            return withContext(Dispatchers.IO) {
//                var grupe = ApiAdapter.retrofit.getGrupa()
//                for(i in grupe.indices) {
//                    db!!.grupaDao().insert(grupe[i])
//                }
////                var kvizovi = ApiAdapter.retrofit.getKviz()
////                for(i in kvizovi.indices) {
////                    db!!.kvizDao().insert(kvizovi[i])
////                    var pitanja = ApiAdapter.retrofit.getPitanjaFromKviz(kvizovi[i].id)
////                    for(j in pitanja.indices) {
////                        db!!.pitanjeDao().insert(pitanja[j])
////                    }
////                }
//                var predmeti = ApiAdapter.retrofit.getPredmet()
//                for(i in predmeti.indices) {
//                    db!!.predmetDao().insert(predmeti[i])
//                }
//            }
//        }
//
//        private suspend fun obrisiSve() {
//            val db = AppDatabase.getInstance(context)
////                val account: Account = db!!.accountDao().getStudent(acHash)
//            return withContext(Dispatchers.IO) {
//                db!!.grupaDao().deleteAll()
//                db!!.kvizDao().deleteAll()
//                db!!.predmetDao().deleteAll()
//                db!!.pitanjeDao().deleteAll()
//            }
//        }
    }
}