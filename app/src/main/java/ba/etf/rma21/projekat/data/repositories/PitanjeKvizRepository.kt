package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PitanjeKvizRepository {

    companion object {
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun getPitanja(idKviza: Int): List<Pitanje>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getPitanjaFromKviz(idKviza)
                return@withContext response;
            }
        }
    }
}