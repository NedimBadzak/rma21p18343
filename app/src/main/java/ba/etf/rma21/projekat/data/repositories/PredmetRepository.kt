package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.*

class PredmetRepository {

    companion object {
        var predmetiArrayList: List<Predmet> = ArrayList()

        init {
            CoroutineScope(Job() + Dispatchers.IO).launch {
                predmetiArrayList = getAll()!!
            }
        }

        suspend fun getAll(): List<Predmet>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getPredmet()
                return@withContext response;
            }
        }

        suspend fun getGroupsByPredmet(id : Int) : List<Grupa> {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getGrupeByPredmet(id)
                return@withContext response;
            }
        }


    }

}