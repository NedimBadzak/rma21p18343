package ba.etf.rma21.projekat

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.view.FragmentKvizovi
import ba.etf.rma21.projekat.view.FragmentPredmeti
import ba.etf.rma21.projekat.view.KvizoviListAdapter
import ba.etf.rma21.projekat.viewmodel.AccountViewModel
import ba.etf.rma21.projekat.viewmodel.KvizoviListViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*


class MainActivity : AppCompatActivity() {
    var kvizoviFragment: Fragment = FragmentKvizovi.newInstance()
    var predmetiFragment: Fragment = FragmentPredmeti.newInstance()
    lateinit var bottomNavigationView: BottomNavigationView
    val kvizoviListViewModel = KvizoviListViewModel()
    val scope = CoroutineScope(Job() + Dispatchers.IO)
    lateinit var kvizoviListAdapter : KvizoviListAdapter
    val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.kvizovi -> {
                    openFragment(kvizoviFragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.predmeti -> {
                    openFragment(predmetiFragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        scope.launch {
            kvizoviListAdapter = KvizoviListAdapter(kvizoviListViewModel.chooseKviz(0)!!)
        }
        bottomNavigationView = findViewById(R.id.bottomNav)
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomNavigationView.selectedItemId = R.id.kvizovi
        AccountRepository.setContext(this)
        provjeraDeepLink();

    }

    private fun provjeraDeepLink() {
        val uri = intent
        if (uri != null) {
            val hash = uri.getStringExtra("payload")
            if (hash != null) {
                AccountViewModel.postaviHash(hash)
                Toast.makeText(this, "Trenutni hash korisnika: " + hash, Toast.LENGTH_SHORT).show()
            }

        }
        else {
            AccountViewModel.postaviHash(AccountRepository.getHash())
            Toast.makeText(this, "Trenutni hash korisnika: " + "nesto", Toast.LENGTH_SHORT).show()
        }
    }

    fun openFragment(fragment: Fragment) {
        for(i in 0 until supportFragmentManager.fragments.size) supportFragmentManager.beginTransaction().hide(supportFragmentManager.fragments[i]).commit()
        if(!supportFragmentManager.fragments.contains(fragment)) supportFragmentManager.beginTransaction().add(R.id.container, fragment, fragment.javaClass.name).commit()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.show(fragment)
        transaction.addToBackStack(fragment.javaClass.name)
        transaction.commit()
    }

    override fun onBackPressed() {
        if(supportFragmentManager.getBackStackEntryAt(supportFragmentManager.backStackEntryCount-1).name == (supportFragmentManager.getBackStackEntryAt(0)).name) {
            finish()
        } else
            supportFragmentManager.popBackStackImmediate("ba.etf.rma21.projekat.view.FragmentKvizovi",0)
            supportFragmentManager.beginTransaction().show(kvizoviFragment).commit()
        (kvizoviFragment as FragmentKvizovi).reBindNavListener()
        bottomNavigationView.selectedItemId = R.id.kvizovi
        bottomNavigationView.menu.setGroupVisible(R.id.glavnaGrupa, true)
        bottomNavigationView.menu.setGroupVisible(R.id.kvizGrupa, false)
    }
}

