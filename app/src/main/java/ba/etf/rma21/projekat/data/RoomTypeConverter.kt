package ba.etf.rma21.projekat.data

import android.util.Log
import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.*
import java.text.ParseException
import java.text.SimpleDateFormat

class RoomTypeConverter {
    @TypeConverter
    fun fromString(string: String?): List<String> {
        val listType = object : TypeToken<ArrayList<String?>?>() {}.type
        return Gson().fromJson(string, listType)
    }

    @TypeConverter
    fun fromList(lista: List<String?>?): String {
        val gson = Gson()

        return gson.toJson(lista)
    }

//    @TypeConverter
//    fun StringToListaPredmeta(string: String): List<Predmet> {
//        var lista: MutableList<Predmet> = mutableListOf()
//        CoroutineScope(Job() + Dispatchers.Main).launch {
//            var predmeti = PredmetIGrupaRepository.getPredmeti()
//            if (string != null && predmeti != null) {
//                var tempLista = string.split(",")
//                for (i in tempLista.indices) {
//                    for (j in predmeti.indices) {
//                        if (tempLista[i] == predmeti[i].naziv) lista.add(predmeti[i])
//                    }
//                }
//            }
//        }
//        return lista
//    }
//    @TypeConverter
//    fun ListaPredmetaToString(lista: List<Predmet>): String? {
//        var stringer: String = ""
//        var listaNaziva : MutableList<String?> = mutableListOf()
//        if (lista != null) {
//            for (i in lista.indices) {
//                listaNaziva.add(lista[i].naziv)
//            }
//        }
//        return fromList(listaNaziva)
//    }

//    @TypeConverter
//    fun pretvoriDatumKratkiUDugi(string: String?): String {
//        try {
//            SimpleDateFormat("yyyy-mm-dd").parse(string)
//        } catch (e : ParseException) {
//            Log.d("TAGIC", e.toString())
//        }
//
//    }
//
//    @TypeConverter
//    fun pretvoriDatumKratkiUDugi() {
//
//    }


}