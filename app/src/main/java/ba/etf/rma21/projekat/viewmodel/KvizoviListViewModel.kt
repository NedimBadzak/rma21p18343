package ba.etf.rma21.projekat.viewmodel

import androidx.lifecycle.ViewModel
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import kotlinx.coroutines.*

class KvizoviListViewModel : ViewModel() {

    suspend fun chooseKviz(position: Int): List<Kviz> {
        return when (position) {

            0 -> KvizRepository.getUpisani()!!
            1 -> KvizRepository.getAll()!!
            2 -> KvizRepository.getDone()
            3 -> KvizRepository.getFuture()
            4 -> KvizRepository.getNotTaken()
            else -> emptyList()
        }
    }
}