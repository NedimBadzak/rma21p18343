package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Pitanje

@Dao
interface PitanjeDAO {

    @Query("DELETE FROM pitanje")
    suspend fun deleteAll()

    @Insert
    suspend fun insert(pitanje: Pitanje)
}