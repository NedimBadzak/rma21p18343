package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.repositories.AccountRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class AccountViewModel {
    companion object {

        fun postaviHash(acHash: String) {
            CoroutineScope(Job() + Dispatchers.IO).launch {
                AccountRepository.postaviHash(acHash)
            }
        }
    }
}