package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Kviz
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class KvizRepository {
    companion object {
        var date: Date = Calendar.getInstance().time
        var kvizArrayList: List<Kviz> = ArrayList()
        val sdf = SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss")
        init {
            CoroutineScope(Job() + Dispatchers.IO).launch {
                kvizArrayList = getAll()!!
            }
        }
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }
        //Nove metode
        suspend fun getAll(): List<Kviz>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getKviz()
                return@withContext response;
            }
        }

        suspend fun getById(id: Int): Kviz? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getKvizByID(id)
                if (!response.toString().contains("\"message\": \"Kviz not found.\"")) {
                    return@withContext response;
                } else {
                    throw Throwable("Kviz not found")
                }
            }
        }

        suspend fun dajKvizByGrupa(gid: Int): List<Kviz> {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.dajKvizoveByGrupa(gid)
                return@withContext response;
            }
        }

        suspend fun getUpisani(): List<Kviz>? {
            val temp = withContext(Dispatchers.IO) {
                val upisaneGrupe = GrupaRepository.dajUpisaneGrupe()
                val resultLista: MutableList<Kviz> = mutableListOf()
//                val scope = CoroutineScope(Job() + Dispatchers.IO)
                for (i in upisaneGrupe.indices) {
                    resultLista.addAll(dajKvizByGrupa(upisaneGrupe[i].id))
                }
                return@withContext resultLista;
            }
            return temp
        }

        suspend fun getDone(): List<Kviz> {
            val temp = withContext(Dispatchers.IO) {
                val upisaniKvizovi = getUpisani()
                val zapocetiKvizovi = TakeKvizRepository.getPocetiKvizovi()
                val resultLista: MutableList<Kviz> = mutableListOf()
//                val scope = CoroutineScope(Job() + Dispatchers.IO)
                for (i in zapocetiKvizovi!!.indices) {
                    for(j in upisaniKvizovi!!.indices) {
                        if (zapocetiKvizovi[i].KvizId == upisaniKvizovi[j].id) resultLista.add(upisaniKvizovi[j])
                    }
                }
                return@withContext resultLista;
            }
            return temp
        }

        suspend fun getFuture(): List<Kviz> {
            val temp = withContext(Dispatchers.IO) {
                val upisaniKvizovi = getAll()
                val resultLista: MutableList<Kviz> = mutableListOf()
//                val scope = CoroutineScope(Job() + Dispatchers.IO)
                for (i in upisaniKvizovi!!.indices) {
                    if(sdf.parse(upisaniKvizovi[i].datumPocetka).after(date)) resultLista.add(upisaniKvizovi[i])
                }
                return@withContext resultLista;
            }
            return temp
        }

        suspend fun getNotTaken(): List<Kviz> {
            val temp = withContext(Dispatchers.IO) {
                val upisaniKvizovi = getAll()
                val resultLista: MutableList<Kviz> = mutableListOf()
//                val scope = CoroutineScope(Job() + Dispatchers.IO)
                for (i in upisaniKvizovi!!.indices) {
                    if(upisaniKvizovi[i].datumKraj != null && sdf.parse(upisaniKvizovi[i].datumKraj!!).before(date)) resultLista.add(upisaniKvizovi[i])
                }
                return@withContext resultLista;
            }
            return temp
        }


        init { }

    }
}