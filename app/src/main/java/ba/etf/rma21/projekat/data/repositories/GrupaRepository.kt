package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import kotlinx.coroutines.*

class GrupaRepository {
    companion object {
        var grupeArrayList : List<Grupa> = ArrayList()
        var upisaneGrupe : MutableList<Grupa> = ArrayList()

        init {
            CoroutineScope(Job() + Dispatchers.IO).launch {
                upisaneGrupe = dajUpisaneGrupe() as MutableList<Grupa>
            }
        }

        suspend fun dajUpisaneGrupe() : List<Grupa> {
                return withContext(Dispatchers.IO) {
                    val response = ApiAdapter.retrofit.getUpisaneGrupe(AccountRepository.getHash())
                    return@withContext response;
                }
        }
    }
}