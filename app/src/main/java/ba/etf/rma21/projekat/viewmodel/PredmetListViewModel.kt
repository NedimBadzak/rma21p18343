package ba.etf.rma21.projekat.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PredmetListViewModel() : ViewModel() {

    suspend fun dajGrupe(predmet : String): List<String> {

        return PredmetIGrupaRepository.getGrupeNepopunjen(predmet)

    }

    suspend fun dajPredmete(godina: Int): List<String> {

        return PredmetIGrupaRepository.getPredmetNepopunjen(godina)
    }

    suspend fun dodaj(godina: String, predmet: String, grupa: String) {

        PredmetIGrupaRepository.upisiUGrupu(PredmetIGrupaRepository.getGroupIDByName(grupa))
    }
}