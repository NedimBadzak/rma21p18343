package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.*

class PredmetIGrupaRepository {

    companion object {
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun getPredmeti(): List<Predmet>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getPredmet()
                return@withContext response;
            }
        }

        suspend fun getPredmetByID(pid: Int): Predmet? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getPredmetById(pid)
                return@withContext response;
            }
        }


        fun getPredmetiByGodina(godina: Int): List<Predmet> {
            var returnLista: MutableList<Predmet> = mutableListOf()
            CoroutineScope(Job() + Dispatchers.IO).launch {
                returnLista = getPredmeti()!!.filter {
                    it.godina == godina
                }.toMutableList()
            }
            return returnLista
        }

        suspend fun getGroupIDByName(ime: String) : Int {
            return getGrupe()!!.firstOrNull { it: Grupa ->
                it.naziv == ime
            }!!.id
        }

        suspend fun getGrupe(): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getGrupa()
                return@withContext response;
            }
        }

        suspend fun getGrupeZaPredmet(idPredmeta: Int): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getGrupeByPredmet(idPredmeta)
                return@withContext response;
            }
        }

        suspend fun upisiUGrupu(idGrupa: Int): Boolean? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.upisiUGrupu(idGrupa, AccountRepository.getHash())
                return@withContext response.toString().contains("je dodan u grupu")
            }
        }



        suspend fun getUpisaneGrupe(): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getUpisaneGrupe(AccountRepository.getHash())
                return@withContext response;
            }
        }

        suspend fun getPredmetNepopunjen(godina: Int) : List<String> {
            val returnLista = PredmetIGrupaRepository.getPredmeti()!!.filter {
                it.godina == godina
            }.toList().map {
                it.naziv
            }.toList().filter {
                getGrupeNepopunjen(it).isNotEmpty()
            }.toList()
            return returnLista
        }

        suspend fun getGrupeNepopunjen(predmet: String): List<String> {
            var returnLista : MutableList<String> = mutableListOf()
            val predmet = PredmetIGrupaRepository.getPredmeti()!!.first {
                it.naziv == predmet
            }
            returnLista = getGrupeZaPredmet(predmet.id)!!.filter{
                !getUpisaneGrupe()!!.contains(it)
            }.map {
                it.naziv
            }.toMutableList()
            return returnLista
        }

        suspend fun getGrupeByKviz(id: Int): List<Grupa> {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getGrupeByKviz(id)
                return@withContext response;
            }
        }
    }
}