package ba.etf.rma21.projekat.data.models

import androidx.room.Entity

@Entity
data class PitanjeKviz(
    val naziv:String,
    val predmet : String,
    val kviz: Kviz
) {

}
