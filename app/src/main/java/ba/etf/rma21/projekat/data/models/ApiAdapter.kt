package ba.etf.rma21.projekat.data.models

import ba.etf.rma21.projekat.data.repositories.ApiConfig
//import okhttp3.OkHttpClient
//import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
//import java.util.concurrent.TimeUnit

object ApiAdapter {
//    private val interceptor = run {
//        val httpLoggingInterceptor = HttpLoggingInterceptor()
//        httpLoggingInterceptor.apply {
//            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
//        }
//    }
//
//
//    private val okHttpClient = OkHttpClient.Builder()
//        .addInterceptor(interceptor) // same for .addInterceptor(...)
//        .connectTimeout(30, TimeUnit.SECONDS) //Backend is really slow
//        .writeTimeout(30, TimeUnit.SECONDS)
//        .readTimeout(30, TimeUnit.SECONDS)
//        .build()

    val retrofit : ApiService = Retrofit.Builder()
        .baseUrl(ApiConfig.baseURL)
        .addConverterFactory(GsonConverterFactory.create())
//        .client(okHttpClient)
        .build()
        .create(ApiService::class.java)
}