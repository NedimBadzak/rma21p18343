package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.OdgovorPOST
import ba.etf.rma21.projekat.viewmodel.PitanjeViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Math.round

class OdgovorRepository {
    companion object {
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                val kvizTakeni = TakeKvizRepository.getPocetiKvizovi()
                val idKvizTaken = kvizTakeni!!.first {
                    it.KvizId == idKviza
                }
                val response =
                    ApiAdapter.retrofit.getOdgovori(AccountRepository.getHash(), idKvizTaken.id)
                if (response.toString().contains("message")) return@withContext listOf<Odgovor>()
                return@withContext response;
            }
        }

        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int? {
            return withContext(Dispatchers.IO) {
                val pocetiKvizovi = TakeKvizRepository.getPocetiKvizovi()
                val pitanja = PitanjeKvizRepository.getPitanja(
                    TakeKvizRepository.getPocetiKvizovi()!!.firstOrNull {
                        it.id == idKvizTaken
                    }!!.KvizId
                )!!
                var brojPitanja = pitanja.size
                var brojTacnih = pitanja.filter {
                    it.id == idPitanje && it.tacan == odgovor
                }.toList().size
                var prijasnjiBodovi = pocetiKvizovi!!.firstOrNull {
                    it.id == idKvizTaken
                }!!.osvojeniBodovi.toInt()
                var bodovi = round(brojTacnih.toDouble() / brojPitanja * 100).toInt()

                val response = ApiAdapter.retrofit.dodajOdgovor(
                    AccountRepository.getHash(),
                    idKvizTaken,
                    OdgovorPOST(idPitanje, odgovor, bodovi+prijasnjiBodovi)
                )
                PitanjeViewModel.ostvareniBodovi = bodovi+prijasnjiBodovi
                if (!response.toString().contains("message:")) {
                    val temp = TakeKvizRepository.getPocetiKvizovi()!!.firstOrNull {
                        it.id == idKvizTaken
                    }
                    if (temp != null) {
                        var pitanja = PitanjeKvizRepository.getPitanja(temp.KvizId)!!
                        brojPitanja = pitanja.size
                        brojTacnih = pitanja.filter {
                            it.id == idPitanje && it.tacan == odgovor
                        }.toList().size
                        return@withContext round(brojTacnih.toDouble() / brojPitanja * 100).toInt()
                    }
                }
                return@withContext -1
            }
        }
    }
}