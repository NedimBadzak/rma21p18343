package ba.etf.rma21.projekat.data.models

import retrofit2.http.*

interface ApiService {

    //CHANGED
    @GET("/account/{id}/lastUpdate")
    suspend fun getChanged(@Path("id") acHash: String, @Query("date") date: String): String


    //PREDMET citav
    @GET("predmet")
    suspend fun getPredmet(): List<Predmet>

    @GET("/predmet/{id}")
    suspend fun getPredmetById(@Path("id") id: Int): Predmet


    //GRUPA citava
    @GET("/kviz/{id}/grupa")
    suspend fun getGrupeByKviz(@Path("id") id: Int): List<Grupa>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun upisiUGrupu(@Path("gid") idGrupe: Int, @Path("id") acHash: String): Unit

    @GET("/student/{id}/grupa")
    suspend fun getUpisaneGrupe(@Path("id") acHash: String): List<Grupa>

    @GET("grupa")
    suspend fun getGrupa(): List<Grupa>

    @GET("/grupa/{id}")
    suspend fun dajGrupuPoID(@Path("id") id: Int): Grupa

    @GET("/predmet/{id}/grupa")
    suspend fun getGrupeByPredmet(@Path("id") id: Int): List<Grupa>


    //KVIZ citav
    @GET("kviz")
    suspend fun getKviz(): List<Kviz>

    @GET("kviz/{id}")
    suspend fun getKvizByID(@Path("id") id: Int): Kviz

    @GET("/grupa/{id}/kvizovi")
    suspend fun dajKvizoveByGrupa(@Path("id") id: Int): List<Kviz>


    //ODGOVOR citav
    @GET("/student/{id}/kviztaken/{ktid}/odgovori")
    suspend fun getOdgovori(@Path("id") acHash: String, @Path("ktid") ktid: Int): List<Odgovor>

    @Headers("accept: application/json", "Content-Type: application/json")
    @POST("/student/{id}/kviztaken/{ktid}/odgovor")
    suspend fun dodajOdgovor(
        @Path("id") acHash: String,
        @Path("ktid") ktid: Int,
        @Body odgovorPOST: OdgovorPOST
    ): Unit


    //KVIZTAKEN citav
    @GET("/student/{id}/kviztaken")
    suspend fun getKvizTakenList(@Path("id") acHash: String): List<KvizTaken>

    @POST("/student/{id}/kviz/{kid}")
    suspend fun zapocniOdgovaranjeKviza(
        @Path("id") acHash: String,
        @Path("kid") kid: Int
    ): KvizTaken

    //ACCOUNT citav
    @GET("/student/{id}")
    suspend fun getStudent(@Path("id") acHash: String): Account

    @DELETE("/student/{id}/upisugrupeipokusaji")
    suspend fun obrisiSveZaStudenta(@Path("id") acHash: String): Boolean

    //PITANJE citav
    @GET("/kviz/{id}/pitanja")
    suspend fun getPitanjaFromKviz(@Path("id") id: Int): List<Pitanje>


}