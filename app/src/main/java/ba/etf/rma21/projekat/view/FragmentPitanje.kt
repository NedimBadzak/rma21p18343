package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.viewmodel.PitanjeViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class FragmentPitanje(
    var pitanje: Pitanje
) : Fragment() {

    var odgovoriLista : ListView? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_pitanje, container, false)
        odgovoriLista = view.findViewById(R.id.odgovoriLista)
        val tekstPitanja : TextView = view.findViewById(R.id.tekstPitanja)
        retainInstance = true
        odgovoriLista?.adapter = ArrayAdapter(
            context!!,
            android.R.layout.simple_list_item_1,
            pitanje.opcije.split(",")
        )

        if(savedInstanceState != null) {
            Toast.makeText(context, "Treba oznaciti ${savedInstanceState.getInt("odgovor")}", Toast.LENGTH_SHORT).show()
        }
        if(PitanjeViewModel.listaZavrsenih.containsKey(PitanjeViewModel.pokrenutiKviz)) {
            if(PitanjeViewModel.kvizPitanjaRezultati[PitanjeViewModel.pokrenutiKviz]?.get(pitanje) == true)
                (odgovoriLista?.adapter!!.getView(pitanje.tacan-1, null, null) as TextView).setTextColor(resources.getColor(R.color.zeleniodgovor, null))
            else
                odgovoriLista?.adapter!!.getView(if(pitanje.tacan-1==0) 1 else 0, null, null).setBackgroundColor(resources.getColor(R.color.crveniodgovor, null))

        }
        odgovoriLista?.setOnItemClickListener { parent, view, position, id ->
            if(position == pitanje.tacan) {
                    (view as TextView).setTextColor(resources.getColor(R.color.zeleniodgovor, null))
                parent.isEnabled = false
                odgovor = true
                PitanjeViewModel.pitanjaOdgovori.put(pitanje, true)
                CoroutineScope(Job() + Dispatchers.IO).launch {
                    OdgovorRepository.postaviOdgovorKviz(
                        PitanjeViewModel.pokusaj,
                        pitanje.id,
                        position
                    )
                }
                //TODO:Zadnji pasus postavke citav
            }
            else {
                (view as TextView).setTextColor(resources.getColor(R.color.crveniodgovor, null))
                parent.isEnabled = false
                odgovor = false
                PitanjeViewModel.pitanjaOdgovori.put(pitanje, false)
            }
        }
        tekstPitanja.setText(pitanje.tekstPitanja)
        return view
    }

    companion object {
        fun newInstance(pitanje: Pitanje): FragmentPitanje = FragmentPitanje(pitanje)
        var odgovor : Boolean? = null
    }

    override fun onPause() {
        super.onPause()
        this.activity?.supportFragmentManager!!.saveFragmentInstanceState(this)
    }
}

