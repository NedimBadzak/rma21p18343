package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.forEach
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.viewmodel.PitanjeViewModel
import com.google.android.material.navigation.NavigationView


class FragmentPokusaj(
    var listaPitanja: List<Pitanje>
) : Fragment() {

    lateinit var navigacijaPitanja: NavigationView
    var zadnjiKojiJeBio: Int = 0
    var rezultatFragment : FragmentPoruka = FragmentPoruka("Poruka Fragment")
    var fragmentPitanja: MutableList<FragmentPitanje> = mutableListOf()
    private val mNavListener = NavigationView.OnNavigationItemSelectedListener {
        val transaction = this.activity?.supportFragmentManager!!.beginTransaction()
        var index : Int = 0
        if(!it.title.equals("Rezultat")) index = it.title.toString().toInt() - 1
        var prikazivalo : Fragment = fragmentPitanja[index]
        for (i in fragmentPitanja.indices) {
            if (!fragmentPitanja[i].isHidden) {
                transaction.hide(fragmentPitanja[i])
                if (!PitanjeViewModel.listaZavrsenih.containsKey(PitanjeViewModel.pokrenutiKviz)) {
                    if (PitanjeViewModel.pitanjaOdgovori.contains(listaPitanja[i]) &&
                        FragmentPitanje.odgovor != null) {
                        val porukaFragment: Fragment? =
                            this.activity?.supportFragmentManager!!.fragments.find { it1 ->
                                it1 is FragmentPoruka
                            }
                        if (porukaFragment != null && porukaFragment.isVisible) {
                            transaction.hide(porukaFragment)
                            transaction.commit()
                            continue
                        }
                        PitanjeViewModel.kvizPitanjaRezultati.put(PitanjeViewModel.pokrenutiKviz, PitanjeViewModel.pitanjaOdgovori)
                        val item = navigacijaPitanja.menu.getItem(i)
                        val s = SpannableString(item.title)
                        val boja =
                            if (FragmentPitanje.odgovor!!) resources.getColor(
                                R.color.zeleniodgovor,
                                null
                            )
                            else resources.getColor(R.color.crveniodgovor, null)
                        s.setSpan(ForegroundColorSpan(boja), 0, s.length, 0)
                        item.title = s
                        item.isEnabled = false
                        FragmentPitanje.odgovor = null
                    }
                }
            } else if(PitanjeViewModel.listaZavrsenih[PitanjeViewModel.pokrenutiKviz] == true) {
                navigacijaPitanja.menu.forEach { it2 : MenuItem -> it2.isEnabled = true
                    val porukaFragment: Fragment? =
                        this.activity?.supportFragmentManager!!.fragments.find { it1 ->
                            it1 is FragmentPoruka
                        }
                    rezultatFragment = porukaFragment as FragmentPoruka
                    val trans2 = this.activity?.supportFragmentManager!!.beginTransaction()
                    trans2.hide(porukaFragment!!)
                    trans2.commit()
                }
                if(it.title.equals("Rezultat")) {
                    for(j in 0 until fragmentPitanja.size) {
                        if(!fragmentPitanja[j].isHidden) this.activity?.supportFragmentManager!!.beginTransaction().hide(fragmentPitanja[j]).commit()
                    }
                    prikazivalo = rezultatFragment
                }
            }
        }
        if(!activity?.supportFragmentManager?.fragments?.contains(prikazivalo)!!) activity?.supportFragmentManager?.beginTransaction()?.add(R.id.framePitanje, prikazivalo)?.commit()
        transaction.show(prikazivalo)
        zadnjiKojiJeBio = index
        transaction.commit()
        return@OnNavigationItemSelectedListener true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_pokusaj, container, false)
        navigacijaPitanja = view.findViewById<NavigationView>(R.id.navigacijaPitanja)
        for (i in listaPitanja.indices) {
            navigacijaPitanja.menu.add(R.id.navGrupa, i, i, "${i + 1}")
            fragmentPitanja.add(FragmentPitanje.newInstance(listaPitanja[i]))
            Log.v("TAGIC", listaPitanja[i].naziv)
        }
        activity?.supportFragmentManager?.beginTransaction()?.add(R.id.framePitanje, fragmentPitanja[0])?.show(fragmentPitanja[0])?.commit()
        navigacijaPitanja.setNavigationItemSelectedListener(mNavListener)

        return view;
    }

    override fun onResume() {
        super.onResume()
        navigacijaPitanja.setNavigationItemSelectedListener(mNavListener)
    }

}