package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.KvizTaken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TakeKvizRepository {
    companion object {
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun zapocniKviz(idKviza: Int): KvizTaken? {
            return withContext(Dispatchers.IO) {
                val response =
                    ApiAdapter.retrofit.zapocniOdgovaranjeKviza(
                        AccountRepository.getHash(),
                        idKviza
                    )
                return@withContext response;
            }
        }

        suspend fun getPocetiKvizovi(): List<KvizTaken>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getKvizTakenList(AccountRepository.getHash())
                if(response.isEmpty()) return@withContext null
                else return@withContext response;
            }
        }
    }
}