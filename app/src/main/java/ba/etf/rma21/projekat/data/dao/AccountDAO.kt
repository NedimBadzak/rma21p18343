package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Account

@Dao
interface AccountDAO {


//    @Insert
//    suspend fun insertAll(vararg accounts: Account)
//
//    @Delete
//    suspend fun delete(movie: Account)

//    @Transaction
//    @Query("SELECT * FROM movie")
//    suspend fun getMovieAndCast():List<MovieWithCast>

    @Query("SELECT * FROM account WHERE acHash=:acHash")
    suspend fun getStudent(acHash: String): Account

    @Insert
    suspend fun upisi(account: Account)

    @Query("DELETE FROM account")
    suspend fun deleteAll()

    @Delete
    suspend fun obrisiKorisnika(account: Account)


}