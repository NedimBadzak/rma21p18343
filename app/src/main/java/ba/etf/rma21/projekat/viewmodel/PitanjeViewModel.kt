package ba.etf.rma21.projekat.viewmodel

import androidx.lifecycle.ViewModel
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository

class PitanjeViewModel : ViewModel() {
    private var pitanjaKvizRepository = PitanjeKvizRepository()
    companion object {
        var pitanjaOdgovori : MutableMap<Pitanje, Boolean> = mutableMapOf()
        var kvizPitanjaRezultati : MutableMap<Kviz, MutableMap<Pitanje, Boolean>> = mutableMapOf()
        var listaZavrsenih : MutableMap<Kviz, Boolean> = mutableMapOf()
        lateinit var pokrenutiKviz : Kviz
        var pokusaj : Int = 0
        var ostvareniBodovi = 0
    }
}