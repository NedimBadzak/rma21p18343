package ba.etf.rma21.projekat

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import org.hamcrest.CoreMatchers
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MySpirala2AndroidTest {
/*
    @get:Rule
    val intentsTestRule = IntentsTestRule<MainActivity>(MainActivity::class.java)

    @Test
    fun radiZaustaviKviz() {
        Espresso.onView(withId(R.id.filterKvizova)).perform(ViewActions.click())
        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Svi moji kvizovi"))).perform(
            ViewActions.click()
        )
        val kvizovi = KvizRepository.getMyKvizes()
        Espresso.onView(withId(R.id.listaKvizova)).perform(
            RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(
                CoreMatchers.allOf(
                    hasDescendant(withText(kvizovi[0].naziv)),
                    hasDescendant(withText(kvizovi[0].nazivPredmeta))
                ), ViewActions.click()
            ))
        Espresso.onView(withId(R.id.navigacijaPitanja))
            .check(matches(isDisplayed()))
        Espresso.onView(withId(R.id.zaustaviKviz)).perform(ViewActions.click())
        Espresso.onView(withId(R.id.listaKvizova))
            .check(matches(isDisplayed()))
    }

    @Test
    fun prikazujeSeFragmentPoruka() {
        Espresso.onView(withId(R.id.filterKvizova)).perform(ViewActions.click())
        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`("Svi moji kvizovi")
            )
        ).perform(ViewActions.click())
        val kvizoviPrije = KvizRepository.getMyKvizes()
        Espresso.onView(withId(R.id.listaKvizova)).check(
            UtilTestClass.hasItemCount(
                kvizoviPrije.size
            )
        )
        for (kviz in kvizoviPrije) {
            UtilTestClass.itemTest(R.id.listaKvizova, kviz)
        }
        Espresso.onView(withId(R.id.predmeti)).perform(ViewActions.click())
        Espresso.onView(withId(R.id.odabirGodina)).perform(ViewActions.click())
        val nedodjeljeniKvizovi = KvizRepository.getAll().minus(KvizRepository.getMyKvizes())
        val nedodjeljeniPredmeti = PredmetRepository.getAll().minus(PredmetRepository.getUpisani())

        var grupaVrijednost = ""
        var predmetNaziv = ""
        var godinaVrijednost = -1
        for (nk in nedodjeljeniKvizovi) {
            for (np in nedodjeljeniPredmeti) {
                if (nk.nazivPredmeta == np.naziv) {
                    grupaVrijednost = nk.nazivGrupe
                    godinaVrijednost = np.godina
                    predmetNaziv = np.naziv

                }
            }
        }
        assertThat(
            "Nema neupisanih predmeta sa kvizovima",
            godinaVrijednost,
            CoreMatchers.not(CoreMatchers.`is`(-1))
        )

        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(godinaVrijednost.toString())
            )
        ).perform(ViewActions.click())
        Espresso.onView(withId(R.id.odabirPredmet)).perform(ViewActions.click())
        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(predmetNaziv)
            )
        ).perform(ViewActions.click())
        Espresso.onView(withId(R.id.odabirGrupa)).perform(ViewActions.click())
        Espresso.onData(
            CoreMatchers.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(grupaVrijednost)
            )
        ).perform(ViewActions.click())
        Espresso.onView(withId(R.id.dodajPredmetDugme)).perform(ViewActions.click())
        Espresso.onView(withSubstring("Uspješno ste upisani u grupu")).check(matches(isDisplayed()))
    }

 */
}
